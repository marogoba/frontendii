
// 1. Crie um `objeto literal` para armazenar os itens considerando as propriedades: `id`, `tarefa`, `autor`.

// - 1.1. Crie um método no `objeto literal` que captura a data e hora atual e armaneze na propriedade `data`

// 2. Converta o `objeto literal` em um `json`.

// 3. Armazene o `json` no `localStorage` ou `sessionStorage`.

// 4. Valide se existe informações no `localStorage`:

// - 4.1. Caso exista, as apresente.
var formulario = document.forms['formularioDeCadastro'];
var objjson = localStorage.getItem("ObjetoLiteral");
var item = document.querySelector(".c-form__campo");
var listaItem = document.querySelector(".c-lista__item")

formulario.addEventListener("submit", function(evento) {

   
    // Captura do valor do campo nome
    var valordoCampoItem = evento.target[0].value.trim();
    var campoPossuiCaracteresEspeciais = possuiCaracteresEspeciais(valordoCampoItem);

    if(campoPossuiCaracteresEspeciais) {
        alert('Não é permitido o uso de caracteres especiais em sua lista.');
    } else {
        listaItem.innerHTML = valordoCampoItem;
    }
    evento.preventDefault();

    // 4. Valide se existe informações no `localStorage`:
    // - 4.1. Caso exista, as apresente. 
    if (localStorage.getItem("ObjetoLiteral") != null) {
        console.log('No LocalStorge \n', localStorage.getItem("ObjetoLiteral"));
    } else {
    // 1. Crie um objeto literal para armazenar os iten s considerando as propriedades: id, tarefa, autor.
    // - 1.1. Crie um método no `objeto literal` que captura a data e hora atual e armaneze na propriedade `data`
        var objetoLiteral = {
            id: 0, 
            titulo: "Como o seu primeiro milhão", 
            autor: "Carlos Silva",
            data: "",
            novaData: function() {
            let auxData = new Date();
            this.data = auxData.toLocaleDateString() + " " + auxData.toLocaleTimeString();    
            }
        };
        objetoLiteral.novaData();
    // 2. Converta o objeto literal em um json.
        var json = JSON.stringify(objetoLiteral);
    // 3. Armazene o json no localStorage ou sessionStorage.
        localStorage.setItem("ObjetoLiteral", json);
    };
   
});

let btn = document.querySelector(".c-form__botao");

item.addEventListener('blur', validarItemDoFormulario);

function validarItemDoFormulario(evento) {
    
    // Resultado das condicionais de validação (retornará true ou false)
    var campoEstaVazio = estaVazio(evento.target.value.trim());
    console.log(campoEstaVazio);
    
    // Condicionais de validação para apresentação das mensagens para o usuário
    if(campoEstaVazio) {
        btn.disabled = true;
    } else  {
        btn.disabled = false;
    };
}

function estaVazio(valorDoCampo) {

    var respostaDaValidacao = valorDoCampo === '';
    
    return respostaDaValidacao;
}

function possuiCaracteresEspeciais(valorDoCampo) {

    var expressaoRegular = /[@!#$%^&*()/\\]/;
        
    var respostaDaValidacao = expressaoRegular.test(valorDoCampo);

    return respostaDaValidacao;
};



