
/* 

    1. Selecione o formulário e o botão do campo de entrada


    2. Crie uma rotina que será disparada a partir do evento de envio

        - 2.1. Remova todos os espaços vazios no início e no fim do texto

        - 2.2. Valide se existem caracteres especiais

            - Se o campo possuir caracteres especiais, apresente um alerta para o usuário com a seguinte mensagem `"Não é permitido o uso de caracteres especiais em sua lista."`. **Nota:** Pode ser utilizado o método `alert()` para simplificar a lógica.

        - 2.3. Caso passe na validacão, apresente o item no elemento DOM `<li class="c-lista__item">`.

    3. Crie uma rotina que será disparada sempre que o usuário tirar o foco da campo de entrada

        - 2.1. Valide se o campo esta vazio

            - Se o campo estiver vazio, desative o botão `<button class="c-form__botao u-my" type="submit">Adicionar item</button>`.

        - 2.2. Valide se existem caracteres especiais 

            - Se o campo possuir caracteres especiais, apresente a seguinte mensagem no console do navegador `"Não é permitido o uso de caracteres especiais em sua lista."`.
*/

var formulario = document.querySelector("#formularioDeCadastro");
var item = document.querySelector(".c-form__campo");
var lista = document.querySelector(".c-lista");
var li = document.querySelector(".c-lista__item");

formulario.addEventListener("submit", function (evento) {

    var elemento = document.createElement("li");
    
    // Captura do valor do campo nome
    var valordoCampoItem = evento.target[0].value.trim();
    var campoPossuiCaracteresEspeciais = possuiCaracteresEspeciais(valordoCampoItem);

    if(campoPossuiCaracteresEspeciais) {
        alert('Não é permitido o uso de caracteres especiais em sua lista.');
    } else {
        let adicionaitem = document.createTextNode(valordoCampoItem); 
        elemento.appendChild(adicionaitem);
        lista.appendChild(elemento);
        li.remove();
    }
    evento.preventDefault();
        
});

let btn = document.querySelector(".c-form__botao");
item.addEventListener('blur', validarItemDoFormulario);

function validarItemDoFormulario(evento) {
    
    // Resultado das condicionais de validação (retornará true ou false)
    var campoEstaVazio = estaVazio(evento.target.value.trim());
    //var campoPossuiNumeros = possuiNumeros(valorDoCampoNome);
    //var campoPossuiCaracteresEspeciais = possuiCaracteresEspeciais(valorDoCampoNome);
    //var campoPossuiMaisDeVinteCaracteres = possuiMaisDeVinteCaracteres(valorDoCampoNome)
    
    // Condicionais de validação para apresentação das mensagens para o usuário
    if(campoEstaVazio) {
        btn.disabled = true;
    } else if (campoPossuiCaracteresEspeciais) {
        alert('Não é permitido o uso de caracteres especiais em sua lista.')
    } else {
        btn.disabled = false;
    };
}

function estaVazio(valorDoCampo) {

    var respostaDaValidacao = valorDoCampo === '';
    
    return respostaDaValidacao;
}

function possuiCaracteresEspeciais(valorDoCampo) {

    var expressaoRegular = /[@!#$%^&*()/\\]/;
        
    var respostaDaValidacao = expressaoRegular.test(valorDoCampo);

    return respostaDaValidacao;
};