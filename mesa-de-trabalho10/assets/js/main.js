
// 1. Selecione o formulário
var formulario = document.querySelector("#formularioDeCadastro");
var lista = document.querySelector(".c-lista");
var li = document.querySelector(".c-lista__item");


// 2. Crie da rotina que será disparada a partir do evento de envio
// 3. Capture o evento de envio do formulário e disparar a rotina
formulario.addEventListener("submit", function (evento) {
    var elemento = document.createElement("li");
    // Captura o valor do input
    let item = evento.target[0].value;
    // 2.2. Remova todos os espaços vazios
    // 2.1. Remova todos os números permitindo apenas texto
    item = item.replace(/\s+/g, "").replace(/\d+/g, "");
    let adicionaitem = document.createTextNode(item); 
    elemento.appendChild(adicionaitem);
    lista.appendChild(elemento);
    li.remove();
    evento.preventDefault();
    
});

